package ru.tfs.reactive.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import ru.tfs.reactive.domain.Car;

public interface CarsRepository extends ReactiveCrudRepository<Car, Long> {
}
