package ru.tfs.reactive.annotated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.tfs.reactive.domain.Car;
import ru.tfs.reactive.repository.CarsRepository;

@RestController
@RequestMapping("/api/v1/cars")
public class CarsRestController {

    private final CarsRepository repository;

    @Autowired
    public CarsRestController(CarsRepository carsRepository) {
        this.repository = carsRepository;
    }

    @GetMapping
    public Flux<Car> getCars() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Car> getCar(@PathVariable Long id) {
        return repository.findById(id);
    }

    @PostMapping
    public Mono<Car> createCar(@RequestBody Car car) {
        return repository.save(car);
    }
}
